<link href='<?php echo UAP_URL . 'assets/css/parsrix-panel.css';?>' rel='stylesheet' type='text/css' />
<div class="row">
    <div class="col-sm-8">
        <div class="dashboard-icons mt-4 my-1 rounded bg-white bordered">
            <div class="card-header">
                <h4><?php echo __('Balance status', 'uap'); ?></h4>
                <hr>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 text-center">
                    <section class="chart-wrapper">
                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">
                            <circle class="circle-chart__background" stroke="#efefef" stroke-width="2" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                            <circle class="circle-chart__circle" stroke="#00acc1" stroke-width="2" stroke-dasharray="<?php echo $data['achieved']; ?>,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                            <g class="circle-chart__info">
                                <text class="circle-chart__subline" x="16.91549431" y="20.5" alignment-baseline="central" text-anchor="middle" font-size="2"><?php echo __('Until the next Rank...', 'uap'); ?></text>
                            </g>
                        </svg>
                        <img class="icon-badge" src="<?php echo UAP_URL.'assets/images/diamond.svg'; ?>" width="35px" alt="">
                    </section>
                </div>
                <div class="col-xs-6 col-sm-4 text-center">
                    <i class="fal fa-wallet fa-4x"></i>
                    <span class="d-block"><?php echo __('Your current Balance', 'uap'); ?></span>
                    <h3 class="currency"><?php echo round($data['stats']['unpaid_payments_value'], 2); ?></h3>
                </div>
                <div class="col-xs-6 col-sm-4 text-center">
                    <a href="?ihc_ap_menu=payments">
                        <div class="cash-btn">
                            <i class="fal fa-credit-card fa-4x"></i>
                            <span class="d-block"><?php echo __('request', 'uap');?></span><span class="d-block"><?php echo __('checkout', 'uap');?></span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="dashboard-icons mt-4 my-1 rounded bg-white bordered">
          <div class="card-header">
              <h4><?php echo __('Invite status', 'uap'); ?></h4>
              <hr>
          </div>
            <div class="row">
                <div class="col-xs-6 text-center">
                    <i class="fal fa-hands-helping fa-4x"></i>
                    <span class="d-block"><?php echo __('Paid Referrals', 'uap'); ?></span>
                    <h3 class="person"><?php echo $data['stats']['paid_referrals_count']; ?></h3>
                </div>
                <div class="col-xs-6 text-center">
                    <i class="fal fa-envelope-open-text fa-4x"></i>
                    <span class="d-block"><?php echo __('Total Referrals', 'uap'); ?></span>
                    <h3 class="person"><?php echo $data['stats']['referrals']; ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>
