<link href='<?php echo UAP_URL . 'assets/css/parsrix-panel.css';?>' rel='stylesheet' type='text/css' />
<div class="row">
    <div class="col-xs-12 col-md-4">
        <div class="money-req dashboard-icons bg-white">
            <!--<form action="#">-->
<!--                <input class="form-control m-1" type="text" 
                       placeholder="<?php //echo __('withdraw amount', 'uap');?>">-->
<!--                <button class="btn btn-success m-1" type="submit">
                    <?php //echo __('withdraw', 'uap');?>
                    <i class="fal fa-credit-card"></i>
                </button>-->
            <a href="#" class="btn btn-success m-1" data-toggle="modal" data-target="#contactModal">
                <?php echo __('withdraw', 'uap');?>
                <i class="fal fa-credit-card"></i>
            </a>
            <!--</form>-->
        </div>
        <div class="acc-details dashboard-icons bg-white">
            <ul>
                <li>
                    <h4><?php echo __('balance:', 'uap');?></h4>
                    <span class="currency">
                        <?php echo 
                        round($data['stats']['unpaid_payments_value'], 2);?>
                    </span>
                </li>
                <?php $authenticated = TRUE; ?>
                <?php foreach ($data['bank_info'] as $item_name=>$item_value):?>
                <li>
                    <h4><?php echo $item_name;?></h4>
                    <?php if(empty($item_value)){
                        $item_value = __('not yet registered', 'uap');
                        $authenticated = FALSE;
                    }
                    ?>
                    <span><?php echo $item_value;?></span>
                </li>
                <?php endforeach;?>
            </ul>
        </div>
        <div class="identify dashboard-icons bg-white">
<!--            <a href="#" class="btn btn-primary m-1">
            <?php //echo __($authenticated ? 
                    //'edit financial information':'submit financial information',
                    //'uap');
            ?></a>-->
            <a href="#" class="btn btn-primary m-1" data-toggle="modal" data-target="#contactModal">
                <?php echo __($authenticated ? 
                    'edit financial information':'submit financial information',
                    'uap');
                ?>
            </a>
        </div>
        <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">توجه</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body contact-payments">
                  <div class="row px-2">
                    <div class="col-12">
                      <img class="mx-auto img-fluid" src="<?php echo UAP_URL . 'assets/images/pose.png';?>">
                    </div>
                    <div class="col-12">
                      <p>با سپاس از شما کاربر گرامی، جهت تکمیل اطلاعات مالی خود و دریافت وجه لطفا با شماره ذیل تماس حاصل فرمایید. </p>
                      <h4>۰۹۹۱۳۰۶۸۱۵۳۶ </h4>
                    </div>
                  </div>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <strong>نکته :</strong>به زودی این قسمت مکانیزه خواهد شد. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                  <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
              </div>
            </div>
        </div>
    </div>
    <!-- main content -->
    <div class="col-xs-12 col-md-8">
        <div class="main-payment dashboard-icons bg-white">
            <?php echo $data['filter'];?>
            <div class="uap-wrapper">
                    <?php if (!empty($data['listing_items'])) : ?>
                <table class="uap-account-table">
                    <thead>
                        <tr>
                            <th><?php _e('Amount', 'uap');?></th>
                            <th><?php _e('Create Date', 'uap');?></th>
                            <th><?php _e('Update Date', 'uap');?></th>
                            <th><?php _e('Status', 'uap');?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>	
                            <th><?php _e('Amount', 'uap');?></th>
                            <th><?php _e('Create Date', 'uap');?></th>
                            <th><?php _e('Update Date', 'uap');?></th>
                            <th><?php _e('Status', 'uap');?></th>
                        </tr>
                    </tfoot>
                    <tbody class="uap-alternate">
                        <?php foreach ($data['listing_items'] as $key => $array): ?>
                        <tr>
                            <td style="font-weight:bold;">
                                <?php 
                                echo uap_format_price_and_currency(
                                        $array['currency'], 
                                        $array['amount']
                                    );?>
                            </td>
                            <td><?php echo uap_convert_date_to_us_format($array['create_date']);?></td>
                            <td><?php echo uap_convert_date_to_us_format($array['update_date']);?></td>
                            <td class="uap-special-label"><?php
                            switch ($array['status']){
                                case 0:
                                        ?>
                                <div><?php _e('Fail', 'uap');?></div>
                                        <?php 								
                                        break;
                                case 1:
                                        ?>
                                <div><?php _e('Pending', 'uap');?></div>
                                        <?php 
                                        break;
                                case 2:
                                        ?>
                                <div><?php _e('Complete', 'uap');?></div>
                                        <?php 									
                                        break;
                            }?>
                            </td>
                        </tr>				
                        <?php endforeach;?>
                    </tbody>
                </table>
                <?php endif;?>

                <?php if (!empty($data['pagination'])):?>
                        <?php echo $data['pagination'];?>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
