<link href='<?php echo UAP_URL . 'assets/css/parsrix-panel.css';?>' rel='stylesheet' type='text/css' />
<link href="<?php echo UAP_URL . 'assets/css/pnotify.custom.css';?>" rel='stylesheet' type='text/css'>
<link href="<?php echo UAP_URL . 'assets/css/animate.css';?>" rel='stylesheet' type='text/css'>
<link href="<?php echo UAP_URL . 'assets/css/invitation.css';?>" rel='stylesheet' type='text/css'>
<link href="<?php echo UAP_URL . 'assets/css/pnotify.custom.css';?>" rel='stylesheet' type='text/css'>
<div class="row">
    <div class="col-xs-12 col-md-8">
       <div class="qr-section dashboard-icons bg-white">
           <div class="adverse-qr">
               <img class="qr img-fluid" src="<?php echo $data['qr_home'];?>" alt="بارکد تبلیغات" />
                <a  class="" ></a>
               <a class="uap-qr-code-download btn btn-success" href="<?php echo $data['qr_home'];?>" download="<?php echo basename($data['qr_home']);?>">
                    <i class="fal fa-download"></i>
                    <?php _e('Download', 'uap');?>
               </a>
           </div>
           <div class="adverse-link">
               <p>شما میتوانید با ارائه ی این لینک به دوستان و آشنایان  و یا حتی فالوورهای اینستاگرامتون اونا رو عضو سامانه کنید و با هر عضویت کسب در آمد کنید، برای کپی کردن لینک روی آن کلیک کنید</p>
               <a class=" copy-btn bg-info form-control" id="copy_url" target="_blank"><?php echo $data['home_url'];?></a>
           </div>
       </div>
       <div class="dashboard-icons mx-2 my-1 rounded bg-white bordered">
           <div class="row">
               <div class="col-xs-6 col-sm-4 text-center">
                   <i class="fal fa-eye fa-4x"></i>
                   <span class="d-block"><?php echo __('Total Referrals', 'uap'); ?></span>
                   <h4 class="person"><?php echo $data['stats']['referrals']; ?></h4>
               </div>
               <div class="col-xs-6 col-sm-4 text-center">
                   <i class="fal fa-hand-holding-usd fa-4x"></i>
                   <span class="d-block"><?php echo __('Your current Balance', 'uap'); ?></span>
                   <h4 class="currency"><?php echo round($data['stats']['unpaid_payments_value'], 2); ?></h4>
               </div>
               <div class="col-xs-12 col-sm-4 text-center">
                   <a href="?ihc_ap_menu=payments">
                        <div class="cash-btn">
                            <i class="fal fa-credit-card fa-4x"></i>
                            <span class="d-block"><?php echo __('request', 'uap');?></span><span class="d-block"><?php echo __('checkout', 'uap');?></span>
                        </div>
                    </a>
               </div>
           </div>
       </div>
       <div class="invite-table dashboard-icons bg-white">
           <?php if (!empty($data['items']) && is_array($data['items'])):?>
            <div>
            <?php echo $data['filter'];?>
                    <table class="uap-account-table">
                              <thead>
                                    <tr>
                                            <th class="uap-account-referrals-table-col1"><?php _e("Id", 'uap');?></th>
                                            <th class="uap-account-referrals-table-col2"><?php _e("Campaign", 'uap');?></th>
                                            <th class="uap-account-referrals-table-col3"><?php _e("Amount", 'uap');?></th>
                                            <th class="uap-account-referrals-table-col4"><?php _e("From", 'uap');?></th>
                                            <?php if (!empty($data['print_source_details'])):?>
                                                    <th class="uap-account-referrals-table-col5"><?php _e('Source Details', 'uap');?></th>
                                            <?php endif;?>
                                            <th class="uap-account-referrals-table-col6"><?php _e("Description", 'uap');?></th>
                                            <th class="uap-account-referrals-table-col7"><?php _e("Date", 'uap');?></th>
                                            <th class="uap-account-referrals-table-col8"><?php _e("Status", 'uap');?></th>
                                    </tr>
                              </thead>
                              <tbody class="uap-alternate">
                            <?php foreach ($data['items'] as $array) : ?>
                                    <tr>
                                            <td class="uap-account-referrals-table-col1"><?php echo $array['id'];?></td>
                                            <td class="uap-account-referrals-table-col2"><?php
                                                    if ($array['campaign']) {
                                                            echo $array['campaign'];
                                                    } else {
                                                            echo '-';
                                                    }
                                            ?></td>
                                            <td  class="uap-account-referrals-table-col3" style="font-weight:bold; color:#111;"><?php echo uap_format_price_and_currency($array['currency'], $array['amount']);?></td>
                                            <td class="uap-account-referrals-table-col4"><?php echo (empty($array['source'])) ? '' : uap_service_type_code_to_title($array['source']);?></td>
                                            <?php if (!empty($data['print_source_details'])):?>
                                                    <td class="uap-account-referrals-table-col5"><?php
                                                            if ($indeed_db->referral_has_source_details($array['id'])):
                                                                    $url = add_query_arg('reference', $array['id'], $data['source_details_url']);
                                                                    ?>
                                                                    <a href="<?php echo $url;?>" target="_blank"><?php _e('View', 'uap');?></a>
                                                                    <?php
                                                            else :
                                                                    echo '-';
                                                            endif;
                                                    ?></td>
                                            <?php endif;?>
                                            <td class="uap-account-referrals-table-col6"><?php echo $array['description'];?></td>
                                            <td class="uap-account-referrals-table-col7"><?php echo uap_convert_date_to_us_format($array['date']);?></td>
                                            <td class="uap-special-label uap-account-referrals-table-col8"><?php
                                                    if ($array['status']==0){
                                                            _e('Refuse', 'uap');
                                                    } else if ($array['status']==1){
                                                            _e('Unverified', 'uap');
                                                    } else if ($array['status']==2){
                                                            _e('Verified', 'uap');
                                                    }
                                            ?></td>
                                    </tr>
                            <?php endforeach;?>
                            </tbody>
                    </table>
            </div>
            <?php endif;?>

            <?php if (!empty($data['pagination'])):?>
                    <?php echo $data['pagination'];?>
            <?php endif;?>
       </div>
    </div>
    <div class="col-xs-12 col-md-4">
       <div class="dashboard-icons bg-white">
           <div class="baner-section">
               <img class="img-fluid" src="<?php echo $data['banner']->image; ?>" alt="بنر پارس ریکس">
                <a class="uap-qr-code-download btn btn-success" href="<?php echo $data['banner']->image; ?>" download="<?php echo $data['banner']->image; ?>">
                    <i class="fal fa-download"></i>
                    <?php _e('Download', 'uap');?>
               </a>

          </div>
          <div class="baner-description">
           <h3>دوقدم تا پولدار شدن!</h3>
              <p>کمتر از دو قدم تا پولدار شدن فاصله داری، کافیه بنر ما رو دانلود کنیو با هشتگ مخصوص نشر بدی تو فضای مجازی</p>
          </div>
       </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
<script type="text/javascript" src="<?php echo UAP_URL . 'assets/js/pnotify.custom.min.js';?>"></script>
<script type="text/javascript" src="<?php echo UAP_URL . 'assets/js/parsrix-affiliate-script.js';?>" ></script>
<script>
var clipboard = new ClipboardJS('#copy_url', {
    text: function() {
        return '<?php echo $data['home_url'];?>';
    }
});

clipboard.on('success', function(e) {
    new PNotify({
        title: 'با موفقیت کپی شد!',
        text: 'لینک کپی شد، میتونی با رفتن و به محل مورد نظر و زدن paste از اون استفاده کنی',
        type: 'success',
        animate: {
            animate: true,
            in_class: 'flipInX',
            out_class: 'bounceOut'
        }
    });
        });

clipboard.on('error', function(e) {
    new PNotify({
        title: 'اوخ! شرمنده',
        text: 'فک کنم کپی نشد، دوباره سعی کن یا بصورت دستی متن دکمه رو کپی کن',
        type: 'error',
        animate: {
            animate: true,
            in_class: 'flipInX',
            out_class: 'bounceOut'
        }
    });
});
</script>
